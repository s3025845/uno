package Controller;


import java.io.*;
import java.net.Socket;
import java.util.ArrayList;

import Model.Card;
import org.json.JSONArray;
import org.json.JSONObject;
import Model.NetworkProtocol.Command;

//This reads the commands from the client
public class ClientHandler implements Runnable{
    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;

    private String delimiter;


    public ClientHandler (Socket socket) throws IOException{
        this.socket = socket;
        this.in= new BufferedReader(new InputStreamReader(socket.getInputStream()));
        this.out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()),true);
        this.delimiter = " ";
    }


    /**
     * Handles the command that is sent is printed
     */
    @Override
    public void run(){
        try{
            while(true){
                handleCommand();
            }
        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Receives a message from the client and prints the JSONObject with all the corresponding properties.
      * @param msg the message that is sent by the client.
     */
    public void doCommand(String msg) throws IOException{
        out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()),true);
        JSONObject jo=new JSONObject();
        String[] messageSplit=msg.split(delimiter);
        switch(messageSplit[0]){
            case "hi" -> {
                //The hi command that the client sends should have at least 4 arguments.
                if(messageSplit.length>=4){
                    try{
                        String displayName=messageSplit[1];
                        String functionalitiesString=messageSplit[2];
                        String[] functionalitiesArray=functionalitiesString.split(",");
                        JSONArray functionalities = new JSONArray();
                        //Because the size of functionalities is not determined, there should be a check to
                        for(int i=0;i<functionalitiesArray.length;i++){
                            functionalities.put(functionalitiesArray[i]);
                        }
                        String protocolVersion=messageSplit[3];
                        //Adding every variable to the JSONObject
                        jo.put("command",Command.HI);
                        jo.put("displayName",displayName);
                        jo.put("functionalities",functionalities);
                        jo.put("protocolVersion",protocolVersion);
                        out.println(jo);
                    }catch(Exception e){
                        System.out.println("Please enter valid arguments");
                    }
                }
            }
            case "join" -> {
                if(messageSplit.length==2){
                    try{
                        int gameSize=Integer.parseInt(messageSplit[1]);
                        if(gameSize<2||gameSize>10){
                            throw new Exception();
                        }
                        jo.put("command",Command.JOIN_GAME);
                        jo.put("gameSize",gameSize);
                        out.println(jo);
                    }catch(Exception e){
                        System.out.println("Please enter a valid number of players that you want to play with.");
                    }
                }
            }
            case "play" -> {
                if(messageSplit.length ==3){
                    Card.Color color = Card.Color.valueOf(messageSplit[1]);
                    Card.Symbol value =Card.Symbol.valueOf(messageSplit[2]);
                    JSONObject card = new JSONObject();
                    card.put("color", color);
                    card.put("value", value);
                    jo.put("command",Command.PLAY_CARD);
                    jo.put("card",card);
                    out.println(jo);
                }
            }
            case "draw" -> {
                jo.put("command",Command.DRAW_CARD);
                out.println(jo);
            }
        }
    }


    /**
     * Handles the command that is printed by the serverHandler
     */
    public void handleCommand() throws IOException{
        String messageFromServerHandler = in.readLine();
        System.out.println(messageFromServerHandler);
        String[] messageSplit = messageFromServerHandler.split(delimiter);
        Command command = Command.valueOf(String.valueOf(messageSplit[0]));
        JSONObject jo= new JSONObject();
        switch(command){
            case CONNECTED -> {
                System.out.println(("Handshake completed successfully"));
            }
            case QUEUE_JOINED -> {
                String displayName = messageSplit[1];
                String playerNames = messageSplit[2];
                System.out.println(displayName + " has been put into the queue.\n" +
                        "This is what the current queue looks like: "+ playerNames);
            }
            case QUEUE_LEFT -> {
                String displayName = messageSplit[1];
                String playerNames = messageSplit[2];
                System.out.println((displayName + " has left the queue.\n" +
                        "This is what the current queue looks like: "+ playerNames));
            }
            case GAME_STARTED -> {
                System.out.println("A new game has started");
            }
            case ROUND_STARTED -> {
                System.out.println("A new round has started");
            }
            case NEXT_TURN -> {
                String currentPlayer = messageSplit[1];
                System.out.println(currentPlayer + " it is your turn!");
            }
            case ISSUE_CARD -> {
                System.out.println("You have got an extra card");
            }
            case ROUND_FINISHED -> {
                String roundWinner = messageSplit[1];
                System.out.println(roundWinner + " won the round!");
            }
            case GAME_FINISHED -> {
                String gameWinner = messageSplit[1];
                System.out.println(gameWinner + " won the game!");
            }
        }
    }

}