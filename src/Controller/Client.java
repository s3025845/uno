package Controller;

import View.TUI;

import java.io.*;
import java.net.Socket;
import java.util.Arrays;
import java.util.Scanner;

//This sends commands from the client
public class Client implements Runnable{
    private Socket socket;
    private TUI tui;

    public Client(TUI tui){
        this.tui = tui;
    }

    /**
     * Creating a new client and starting a thread
     */
    public static void main(String[] args){
        Client client = new Client(new TUI());
        new Thread(client).start();
    }


    /**
     * Creates a new clientHandler and waits for input to go into the doCommand method
     */
    @Override
    public void run(){
        try{
            socket=new Socket("localhost",2389);
            String message = "";
            Scanner scanner = new Scanner(System.in);
            ClientHandler clientHandler = new ClientHandler(socket);
            new Thread(clientHandler).start();
            tui.printMessage("--------------------------------------Welcome to UNO--------------------------------------");
            tui.printMessage("Type a command");
            do{
                message = scanner.nextLine();
                clientHandler.doCommand(message);
            }while(!message.equals("exit"));
        }catch(IOException e){
            tui.printMessage(Arrays.toString(e.getStackTrace()));
        }
    }


}
