package Controller;

import Model.*;
import Model.NetworkProtocol.Command;
import View.TUI;
import org.json.JSONArray;
import org.json.JSONObject;

import java.awt.*;
import java.io.*;
import java.net.Socket;
import java.util.ArrayList;

public class ServerHandler implements Runnable{

    private Server server;

    private Socket socket;
    private PrintWriter out;

    private BufferedReader in;

    private ArrayList<String> playerNames;
    private String displayName;
    private ArrayList<Player> players;

    private Player player;

    private String protocolVersion;

    public Player getPlayer(){
        return player;
    }


    public ServerHandler(Socket socket,Server server) throws IOException{
        this.server =  server;
        this.socket = socket;
        this.players = new ArrayList<>();
        this.playerNames = new ArrayList<>();
        this.displayName = "";
        this.protocolVersion = "";
        in= new BufferedReader(new InputStreamReader(socket.getInputStream()));
    }

    @Override
    public void run(){
        try{
            out=new PrintWriter(new OutputStreamWriter(socket.getOutputStream()),true);
            System.out.println("A client has connected. Total clients: " + server.getServerHandlerArrayList().size());
            while(true){
                handleCommand();
            }
        }catch(IOException e){
            server.getServerHandlerArrayList().remove(this);
            playerNames.remove(displayName);
            System.out.println("A client has disconnected. Total clients: "+ server.getServerHandlerArrayList().size());
        }
    }

    /**
     * Sends a message to everyone in the ServerHandlerArrayList
     * @param input the message that you want to send.
     */
    public void broadcast( String input){
        for(ServerHandler s: server.getServerHandlerArrayList()){
            s.out.println(input);
        }
    }


    /**
     * Sends a message to a specific ServerHandler
     * @param input the message that you want to send.
     * @param serverHandler the ServerHandler that you want to send the message to.
     */
    public void sendIndividualMessage(String input, ServerHandler serverHandler){
        serverHandler.out.println(input);
    }


    // handleCommand() gets the jo from the client

    /**
     * Gets a JSONObject from the client, gets the command and handles this command.
     */
    public void handleCommand() throws IOException{
        String message = in.readLine();
        JSONArray functionalities = new JSONArray();
        int gameSize = 0;
        JSONObject object = new JSONObject(message);
        Command cmd = object.getEnum(Command.class, "command");
        switch(cmd){
            case HI -> {
                displayName = object.getString("displayName");
                functionalities = object.getJSONArray("functionalities");
                protocolVersion = object.getString("protocolVersion");
                //To make sure everyone works with the same protocol, I check if the protocolVersion is 1.2
                if (!protocolVersion.equals("1.2")){
                    sendIndividualMessage("You should use protocolVersion 1.2! ", this);
                }
                player = new HumanPlayer(displayName, new ArrayList<>(), new TUI(),this);
                sendIndividualMessage(String.valueOf(Command.CONNECTED), this);
            }
            case JOIN_GAME -> {
                if (gameSize == 0){
                    gameSize = object.getInt("gameSize");
                }
                ArrayList<String> names = new ArrayList<>();
                for (ServerHandler serverHandler: server.getServerHandlerArrayList()){
                    if (!serverHandler.displayName.equals("")){
                    names.add(displayName);
                    }
                }
                broadcast((Command.QUEUE_JOINED)+" "+ displayName + " "+names);
                //If there are as many players as the gameSize, a new game will be started.
                if (names.size() == gameSize){
                    broadcast(String.valueOf(Command.GAME_STARTED));
                    server.setGame(new Game(server.getPlayersFromConnections()));
                    new Thread(server.getGame()).start();
                }
            }
            case PLAY_CARD -> {
                JSONObject card = object.getJSONObject("card");
                Card.Color color = Card.Color.valueOf((String)card.get("color"));
                Card.Symbol symbol =(Card.Symbol)card.get("value");
                Card wantToPlay= new Card(color,symbol);
                ArrayList<Card> playerHand = server.getPlayersFromConnections().get(server.getGame().getCurrentPlayerIndex()).getHand();
                if (playerHand.contains(wantToPlay)){
                    server.getPlayersFromConnections().get(server.getGame().getCurrentPlayerIndex()).doMovePlayer(server.getGame().showTopCard());
                }
                broadcast(String.valueOf(Command.NEXT_TURN)+ " "+ players.get(server.getGame().getCurrentPlayerIndex()));
            }
            case DRAW_CARD -> {
                server.getGame().giveCards(server.getPlayersFromConnections().get(server.getGame().getCurrentPlayerIndex()),1);
                broadcast(String.valueOf(Command.ISSUE_CARD));
            }
            default -> {
                if (server.getGame().roundIsOver()){
                    broadcast(String.valueOf(Command.ROUND_FINISHED) + " " + server.getGame().roundWinner());
                }
                if (server.getGame().gameIsOver()){
                    broadcast(String.valueOf(Command.GAME_FINISHED) + " "+ server.getGame().gameWinner());
                }
            }
        }
    }

}
