package Controller;

import Model.Game;
import Model.Player;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;

public class Server implements Runnable{

    private ArrayList<ServerHandler> serverHandlerArrayList;
    private ArrayList<Player> players;
    private Game game;

    public Server(){
        serverHandlerArrayList = new ArrayList<>();
    }

    public ArrayList<ServerHandler> getServerHandlerArrayList(){
        return serverHandlerArrayList;
    }

    public Game getGame(){
        return game;
    }

    public void setGame(Game game){
        this.game=game;
    }


    /**
     * Creates a new Server.
     */
    public static void main(String[] args){
        Server server = new Server();
        new Thread(server).start();
    }

    /**
     * Listens for new connections and starts a new ServerHandler with those new connections.
     */
    @Override
    public void run(){
        System.out.println("--------------------------------------Welcome to UNO--------------------------------------");
        try{
            ServerSocket serverSocket = new ServerSocket(2389);
            while(true){
                Socket socket = serverSocket.accept();
                ServerHandler serverHandler = new ServerHandler(socket, this);
                serverHandlerArrayList.add(serverHandler);
                new Thread(serverHandler).start();
            }

        }catch(IOException e){
            System.out.println(Arrays.toString(e.getStackTrace()));
        }
    }


    /**
     * Creates a list of all the Players that are in the serverHandlerArrayList.
     * @return ArrayList with all the players added.
     */
    public ArrayList<Player> getPlayersFromConnections(){
        ArrayList<Player> result = new ArrayList<>();
        for (ServerHandler serverHandler: serverHandlerArrayList){
            result.add(serverHandler.getPlayer());
        }
        return  result;
    }
}
