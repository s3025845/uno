package Model;

import Controller.ServerHandler;
import View.TUI;

import java.util.ArrayList;

public abstract class Player{

    private String playerName;
    private ArrayList<Card> hand;
    private int points;
    private ServerHandler serverHandler;
    private Game game;

    /**
     * Sends a message to a specific ServerHandler
     * @param input the message that you want to send
     */
    public void sendMessage(String input){
        serverHandler.sendIndividualMessage(input, serverHandler);
    }


    public TUI tui;
    public ArrayList<Card> getHand(){
        return hand;
    }

    public int getPoints(){
        return points;
    }

    /**
     * Constructor gives each player an empty hand
     * @param playerName
     */
    public Player(String playerName, ServerHandler serverHandler){
        this.playerName=playerName;
        hand = new ArrayList<>();
        this.serverHandler = serverHandler;
    }

    public void setPoints(int points){
        this.points += points;
    }

    public String getPlayerName(){
        return playerName;
    }


    /**
     * Returns the card that the player wants to play
     * @param topCard the card that's the highest on the discardPile
     * @return the card that the player wants to play
     */
    public abstract Card doMovePlayer(Card topCard);

    /**
     * Sets the color of a WildCard to a color of choice
     * @param card the card that you want the set the color of
     */
    public abstract void setColorWildcard(Card card);


    /**
     * Lets the player choose whether he wants to play the drawn card
     * @return a String with the answer
     */
    public abstract String  playDrawnCard();

}
